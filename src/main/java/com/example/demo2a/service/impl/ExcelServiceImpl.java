package com.example.demo2a.service.impl;

import com.example.demo2a.dto.request.AddDataRequest;
import com.example.demo2a.dto.response.AddDataRespone;
import com.example.demo2a.entity.ExcelDataB;
import com.example.demo2a.repository.ExcelDataRepository;
import com.example.demo2a.service.ExcelService;
import lombok.extern.log4j.Log4j2;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileInputStream;
import java.util.Iterator;
import java.util.Optional;

@Service
@Log4j2
public class ExcelServiceImpl implements ExcelService {

    @Autowired
    ExcelDataRepository excelDataRepository;

    @Override
    public AddDataRespone addData(AddDataRequest request)throws Exception{
        String responseData = "Data";
        return new AddDataRespone(responseData);
    }

    public void readFromExcel() throws Exception {
        try {
            String filePath = "E:\\Project\\mypoi2.xlsx";

            try (Workbook workbook = new XSSFWorkbook(new FileInputStream(new File(filePath)))) {

                Sheet sheet = workbook.getSheetAt(0);

                Iterator<Row> rowIterator = sheet.iterator();
                while (rowIterator.hasNext()) {
                    Row row = rowIterator.next();

                    if (row.getRowNum() == 0) {
                        continue;
                    }

                    ExcelDataB excelData = new ExcelDataB();

                    excelData.setNhafcungcapdichvukyc(getStringValue(row.getCell(0)));
                    excelData.setCustomerid((int) row.getCell(1).getNumericCellValue());
                    excelData.setData(getStringValue(row.getCell(2)));
                    excelData.setAnhgiattomattruoc(getStringValue(row.getCell(3)));
                    excelData.setAnhgiattomatsau(getStringValue(row.getCell(4)));
                    excelData.setAnhchandung(getStringValue(row.getCell(5)));
                    excelData.setKetquaocrnattruoc(getStringValue(row.getCell(6)));
                    excelData.setThoigianxuly(row.getCell(7).getLocalDateTimeCellValue());
                    excelData.setKetquaocrmatsau(getStringValue(row.getCell(8)));
                    excelData.setThoigianxuly(row.getCell(9).getLocalDateTimeCellValue());
                    excelData.setKetquasosanhmat(getStringValue(row.getCell(10)));
                    excelData.setThoigianxuly(row.getCell(11).getLocalDateTimeCellValue());
                    excelData.setDulieuocrmattruoc(getStringValue(row.getCell(12)));
                    excelData.setDulieuocrmatsau(getStringValue(row.getCell(13)));
                    excelData.setDulieusosanhkhuonmat(getStringValue(row.getCell(14)));

                    excelDataRepository.save(excelData);
                }
            }
        } catch (Exception e) {
            log.error("lỗi khi đọc file");
            throw e;
        }
    }

    private String getStringValue(Cell cell) {
        if (cell == null) {
            return null;
        }

        cell.setCellType(CellType.STRING);
        return cell.getStringCellValue();
    }

    @Override
    public AddDataRespone updateData(AddDataRequest request) throws Exception {
        try {
            Optional<ExcelDataB> optionalExcelData = excelDataRepository.findById(request.getId());

            if (optionalExcelData.isPresent()) {
                ExcelDataB existingData = optionalExcelData.get();
                existingData.setNhafcungcapdichvukyc(request.getNhafcungcapdichvukyc());
                existingData.setCustomerid(request.getCustomerid());
                existingData.setData(request.getData());
                existingData.setAnhgiattomattruoc(request.getAnhgiattomattruoc());
                existingData.setAnhgiattomatsau(request.getAnhgiattomatsau());
                existingData.setAnhchandung(request.getAnhchandung());
                existingData.setKetquaocrnattruoc(request.getKetquaocrnattruoc());
                existingData.setThoigianxuly(request.getThoigianxuly());
                existingData.setKetquaocrmatsau(request.getKetquaocrmatsau());
                existingData.setThoigianxuly1(request.getThoigianxuly1());
                existingData.setKetquasosanhmat(request.getKetquasosanhmat());
                existingData.setThoigianxuly2(request.getThoigianxuly2());
                existingData.setDulieuocrmattruoc(request.getDulieuocrmattruoc());
                existingData.setDulieuocrmatsau(request.getDulieuocrmatsau());
                existingData.setDulieusosanhkhuonmat(request.getDulieusosanhkhuonmat());

                excelDataRepository.save(existingData);

                return new AddDataRespone("Cập nhật dữ liệu thành công");
            } else {
                throw new Exception("Không tìm thấy dữ liệu để cập nhật");
            }
        } catch (Exception e) {
            throw e;
        }
    }

    @Override
    public void deleteData(int id) throws Exception {
        try {
            if (excelDataRepository.existsById(id)) {
                excelDataRepository.deleteById(id);

            } else {
                throw new Exception("Không tìm thấy dữ liệu để xóa");
            }
        } catch (Exception e) {
            throw e;
        }
    }
}



