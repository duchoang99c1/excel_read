package com.example.demo2a.service;

import com.example.demo2a.dto.request.AddDataRequest;
import com.example.demo2a.dto.response.AddDataRespone;

public interface ExcelService{
    AddDataRespone addData(AddDataRequest request) throws Exception;
    void readFromExcel() throws Exception;
    AddDataRespone updateData(AddDataRequest request) throws Exception;
    void deleteData(int id) throws Exception;
}
