package com.example.demo2a.controller;

import com.example.demo2a.dto.request.AddDataRequest;
import com.example.demo2a.service.ExcelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/excelRead")
public class controller {
    @Autowired
    private ExcelService excelService;

    @GetMapping("/read")
    @ResponseBody
    public ResponseEntity<?> readFromExcel() {
        try {
            excelService.readFromExcel();
            return new ResponseEntity<>("đọc excel thành công", HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>("lỗi đọc excel " + e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/add")
    @ResponseBody
    public ResponseEntity<?> addData(@RequestBody AddDataRequest request) {
        try {
            excelService.addData(request);
            return new ResponseEntity<>("Thêm dữ liệu thành công", HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>("Lỗi thêm dữ liệu: " + e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/update")
    @ResponseBody
    public ResponseEntity<?> updateData(@RequestBody AddDataRequest request) {
        try {
            excelService.updateData(request);
            return new ResponseEntity<>("Cập nhật dữ liệu thành công", HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>("Lỗi cập nhật dữ liệu: " + e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/delete/{id}")
    @ResponseBody
    public ResponseEntity<?> deleteData(@PathVariable int id) {
        try {
            excelService.deleteData(id);
            return new ResponseEntity<>("Xóa dữ liệu thành công", HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>("Lỗi xóa dữ liệu: " + e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
