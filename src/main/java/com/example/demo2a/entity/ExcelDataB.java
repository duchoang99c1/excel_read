package com.example.demo2a.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Entity
@Table(name = "infomation")
@Data
public class ExcelDataB implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "data")
    private String data;

    @Column(name = "nhafcungcapdichvukyc")
    private String nhafcungcapdichvukyc;

    @Column(name = "customerid")
    private int customerid;

    @Column(name = "anhgiattomattruoc")
    private String anhgiattomattruoc;

    @Column(name = "anhgiattomatsau")
    private String anhgiattomatsau;

    @Column(name = "anhchandung")
    private String anhchandung;

    @Column(name = "ketquaocrnattruoc")
    private String ketquaocrnattruoc;

    @Column(name = "thoigianxuly")
    private LocalDateTime thoigianxuly;

    @Column(name = "ketquaocrmatsau")
    private String ketquaocrmatsau;

    @Column(name = "thoigianxuly1")
    private LocalDateTime thoigianxuly1;

    @Column(name = "ketquasosanhmat")
    private String ketquasosanhmat;

    @Column(name = "thoigianxuly2")
    private LocalDateTime thoigianxuly2;

    @Column(name = "dulieuocrmattruoc")
    private String dulieuocrmattruoc;

    @Column(name = "dulieuocrmatsau")
    private String dulieuocrmatsau;

    @Column(name = "dulieusosanhkhuonmat")
    private String dulieusosanhkhuonmat;
}
