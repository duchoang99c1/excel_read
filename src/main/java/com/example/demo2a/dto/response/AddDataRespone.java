package com.example.demo2a.dto.response;

import com.example.demo2a.entity.ExcelDataB;
import lombok.Data;

@Data
public class AddDataRespone {
    private ExcelDataB excelData;

    private String message;

    public AddDataRespone(String message) {
        this.message = message;
    }
}
