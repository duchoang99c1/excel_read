package com.example.demo2a.repository;

import com.example.demo2a.entity.ExcelDataB;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ExcelDataRepository extends JpaRepository<ExcelDataB, Integer> {
}
